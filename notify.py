import os
import time
import hmac
import hashlib
import base64
import random

from discord_webhook import DiscordWebhook, DiscordEmbed
from urllib import parse
from settings import log, req


class Notify(object):
    """
    Push all in one
        :param SCKEY: The SCKEY of Server sauce. See the document for details: https://sct.ftqq.com/
        :param COOL_PUSH_SKEY: SKEY of Cool Push. See the document for details: https://cp.xuthus.cc/
        :param COOL_PUSH_MODE: Cool Push push mode. Optional private chat (send), group (group) or WeChat (wx), default: send
        :param BARK_KEY: Bark's IP or device code. For example: https://api.day.app/xxxxxx
        :param BARK_SOUND: Bark's push ringtones. View the list of ringtones in the APP, default: healthnotification
        :param TG_BOT_API: Telegram Bot's api address, used to reverse the proxy Telegram API address.
        :param TG_BOT_TOKEN: Telegram Bot token. Generated when applying for bot from bot father.
        :param TG_USER_ID: The user ID of the Telegram push object.
        :param DD_BOT_TOKEN: The field after the access_token in the Dingding robot WebHook address.
        :param DD_BOT_SECRET: Dingding signature key. On the robot security settings page, the string starting with SEC displayed under the signature column.
        :param WW_BOT_KEY: The field after the key in the WebHook address of the enterprise WeChat robot.
            See the document for details: https://work.weixin.qq.com/api/doc/90000/90136/91770
        :param WW_ID: The corporate ID (corpid) of the company's WeChat. Check it in'Administration Backstage'->'My Company'->'Corporate Information'.
            See the document for details: https://work.weixin.qq.com/api/doc/90000/90135/90236
        :param WW_APP_SECRET: The secret of the enterprise WeChat application. In the'Administration Backstage' ->'Apps and Mini Programs' ->'Apps' ->'Self-built', click into an application to view.
        :param WW_APP_USERID: The user ID of the company's WeChat application push object. In the'Administration background' ->'Contacts', click into a user's details page to view, default: @all
        :param WW_APP_AGENTID: The agentid of the enterprise WeChat application. In the'Administration background' ->'Apps and applets' ->'Apps', click into an application to view.
        :param IGOT_KEY: iGot's KEY. For example: https://push.hellyw.com/xxxxxx
        :param PUSH_PLUS_TOKEN: pushplus one-to-one push or one-to-many push token.
            If PUSH_PLUS_USER is not configured, the default is one-to-one push. For details, see the document: http://pushplus.plus/doc/
        :param PUSH_PLUS_USER: pushplus one-to-many push group code.
            Check it in'One-to-many push'->'Your group' (create new if not available)->'Group code', if you are creating a group, you also need to click "View QR code" to scan and bind , Otherwise the group message cannot be accepted.
        :param PUSH_CONFIG: Custom push configuration in JSON format.
            format:
                {"method":"post","url":"","data":{},"text":"","code":200,"data_type":"data","show_title_and_desp":false, "set_data_title":"","set_data_sub_title":"","set_data_desp":""}
            Description:
                method: required, request method. Default: post.
                url: required, complete custom push link.
                data: Optional, sent data. The default is empty, you can add additional parameters.
                text: required, the key of the status code returned by the response body. For example: the server sauce is errno.
                code: required, the value of the status code returned by the response body. For example, the value of the server sauce is 0.
                data_type: Optional, the method of sending data, optional params|json|data, default: data.
                show_title_and_desp: Optional, whether to merge the title (application name + running status) and the running result. Default: false.
                set_data_title: Required, fill in the key of the message title in the push method data. For example: server sauce is text.
                set_data_sub_title: Optional, fill in the key of the message body in the push method data. The key of the body of some push methods has a secondary structure,
                    Need to cooperate with set_data_title to construct children, mutually exclusive with set_data_desp.
                    For example: In the enterprise WeChat, set_data_title fills in text, set_data_sub_title fills in content.
                set_data_desp: Optional, fill in the key of the message body in the push method data. For example, the server sauce is desp.
                    Mutually exclusive with set_data_sub_title, if both are filled, this item will not take effect.
    """
    # For GitHub Actions: Use the variable in the settings -> secrets of the REPO, and the variable name must be completely consistent with the above parameter variable name, otherwise it will be invalid!!!
    # Name = <Variable Name>, value = <Get Value>
    # Server Chan
    SCKEY = ''
    # Cool Push
    COOL_PUSH_SKEY = ''
    COOL_PUSH_MODE = 'send'
    # iOS Bark App
    BARK_KEY = ''
    BARK_SOUND = '' # Replace '' with 'healthnotification' for default value
    # Telegram Bot
    TG_BOT_API = 'api.telegram.org'
    TG_BOT_TOKEN = ''
    TG_USER_ID = ''
    # DingTalk Bot
    DD_BOT_TOKEN = ''
    DD_BOT_SECRET = ''
    # WeChat Work Bot
    WW_BOT_KEY = ''
    # WeChat Work App
    WW_ID = ''
    WW_APP_SECRET = ''
    WW_APP_USERID = '@all'
    WW_APP_AGENTID = ''
    # iGot Aggregate push
    IGOT_KEY = ''
    # pushplus
    PUSH_PLUS_TOKEN = ''
    PUSH_PLUS_USER = ''
    # Custom Push Config
    PUSH_CONFIG = ''
    # Discord Webhook
    DISCORD_WEBHOOK = ''

    def pushTemplate(self, method, url, params=None, data=None, json=None, headers=None, **kwargs):
        name = kwargs.get('name')
        # needs = kwargs.get('needs')
        token = kwargs.get('token')
        text = kwargs.get('text')
        code = kwargs.get('code')
        if not token:
            log.info(f'{name} 🚫')
            # log.info (f '{name} The {NEDS} needed to push the {NEEDS} is not set, you are skipping ...')
            return
        try:
            response = req.to_python(req.request(
                method, url, 2, params, data, json, headers).text)
            rspcode = response[text]
        except Exception as e:
            # 🚫:disabled; 🥳:success; 😳:fail;
            log.error(f'{name} 😳\n{e}')
        else:
            if rspcode == code:
                log.info(f'{name} 🥳')
            # Telegram Bot
            elif name == 'Telegram Bot' and rspcode:
                log.info(f'{name} 🥳')
            elif name == 'Telegram Bot' and response[code] == 400:
                log.error(f'{name} 😳\n Please take the initiative bot, send a message and check if the TG_USER_ID is right or not')
            elif name == 'Telegram Bot' and response[code] == 401:
                log.error(f'{name} 😳\nTG_BOT_TOKEN error')
            else:
                log.error(f'{name} 😳\n{response}')

    def serverChan(self, text, status, desp):
        SCKEY = self.SCKEY
        if 'SCKEY' in os.environ:
            SCKEY = os.environ['SCKEY']

        if SCKEY.startswith('SCU'):
            url = f'https://sc.ftqq.com/{SCKEY}.send'
            data = {
                'text': f'{text} {status}',
                'desp': desp
            }
        else:
            url = f'https://sctapi.ftqq.com/{SCKEY}.send'
            data = {
                'title': f'{text} {status}',
                'desp': desp
            }
        conf = ['Server Sauce', 'SCKEY', SCKEY, 'errno', 0]
        name, needs, token, text, code  = conf

        return self.pushTemplate('post', url, data=data, name=name, needs=needs, token=token, text=text, code=code)

    def coolPush(self, text, status, desp):
        COOL_PUSH_SKEY = self.COOL_PUSH_SKEY
        if 'COOL_PUSH_SKEY' in os.environ:
            COOL_PUSH_SKEY = os.environ['COOL_PUSH_SKEY']

        COOL_PUSH_MODE = self.COOL_PUSH_MODE
        if 'COOL_PUSH_MODE' in os.environ:
            COOL_PUSH_MODE = os.environ['COOL_PUSH_MODE']

        url = f'https://push.xuthus.cc/{COOL_PUSH_MODE}/{COOL_PUSH_SKEY}'
        data = f'{text} {status}\n\n{desp}'.encode('utf-8')
        conf = ['Cool Push', 'COOL_PUSH_SKEY', COOL_PUSH_SKEY, 'code', 200]
        name, needs, token, text, code  = conf

        return self.pushTemplate('post', url, data=data, name=name, needs=needs, token=token, text=text, code=code)

    def barkPush(self, text, status, desp):
        BARK_KEY = self.BARK_KEY
        if 'BARK_KEY' in os.environ:
            BARK_KEY = os.environ['BARK_KEY']
        
        if BARK_KEY and BARK_KEY.find(
            'https') == -1 and BARK_KEY.find('http') == -1:
            BARK_KEY = f'https://api.day.app/{BARK_KEY}'

        BARK_SOUND = self.BARK_SOUND
        if 'BARK_SOUND' in os.environ:
            BARK_SOUND = os.environ['BARK_SOUND']

        url = f'{BARK_KEY}/{text} {status}/{parse.quote(desp)}'
        data = {
            'sound': BARK_SOUND
        }
        conf = ['Bark App', 'BARK_KEY', BARK_KEY, 'code', 200]
        name, needs, token, text, code  = conf

        return self.pushTemplate('get', url, params=data, name=name, needs=needs, token=token, text=text, code=code)

    def tgBot(self, text, status, desp):
        TG_BOT_TOKEN = self.TG_BOT_TOKEN
        if 'TG_BOT_TOKEN' in os.environ:
            TG_BOT_TOKEN = os.environ['TG_BOT_TOKEN']

        TG_USER_ID = self.TG_USER_ID
        if 'TG_USER_ID' in os.environ:
            TG_USER_ID = os.environ['TG_USER_ID']

        token = ''
        if TG_BOT_TOKEN and TG_USER_ID:
            token = 'token'

        TG_BOT_API = self.TG_BOT_API
        if 'TG_BOT_API' in os.environ:
            TG_BOT_API = os.environ['TG_BOT_API']

        url = f'https://{TG_BOT_API}/bot{TG_BOT_TOKEN}/sendMessage'
        data = {
            'chat_id': TG_USER_ID,
            'text': f'{text} {status}\n\n{desp}',
            'disable_web_page_preview': True
        }
        conf = ['Telegram Bot', 'TG_BOT_TOKEN 和 TG_USER_ID', token, 'ok', 'error_code']
        name, needs, token, text, code  = conf

        return self.pushTemplate('post', url, data=data, name=name, needs=needs, token=token, text=text, code=code)

    def ddBot(self, text, status, desp):
        DD_BOT_TOKEN = self.DD_BOT_TOKEN
        if 'DD_BOT_TOKEN' in os.environ:
            DD_BOT_TOKEN = os.environ['DD_BOT_TOKEN']

        DD_BOT_SECRET = self.DD_BOT_SECRET
        if 'DD_BOT_SECRET' in os.environ:
            DD_BOT_SECRET = os.environ['DD_BOT_SECRET']

        url = ''
        if DD_BOT_TOKEN:
            url = 'https://oapi.dingtalk.com/robot/send?' \
                f'access_token={DD_BOT_TOKEN}'
            if DD_BOT_SECRET:
                secret = DD_BOT_SECRET
                timestamp = int(round(time.time() * 1000))
                secret_enc = bytes(secret).encode('utf-8')
                string_to_sign = f'{timestamp}\n{secret}'
                string_to_sign_enc = bytes(string_to_sign).encode('utf-8')
                hmac_code = hmac.new(
                    secret_enc, string_to_sign_enc,
                    digestmod=hashlib.sha256).digest()
                sign = parse.quote_plus(base64.b64encode(hmac_code))
                url = 'https://oapi.dingtalk.com/robot/send?access_' \
                    f'token={DD_BOT_TOKEN}&timestamp={timestamp}&sign={sign}'

        data = {
            'msgtype': 'text',
            'text': {
                'content': f'{text} {status}\n\n{desp}'
            }
        }
        conf = ['Nail Robot', 'DD_BOT_TOKEN', DD_BOT_TOKEN, 'errcode', 0]
        name, needs, token, text, code  = conf

        return self.pushTemplate('post', url, data=data, name=name, needs=needs, token=token, text=text, code=code)

    def wwBot(self, text, status, desp):
        WW_BOT_KEY = self.WW_BOT_KEY
        if 'WW_BOT_KEY' in os.environ:
            WW_BOT_KEY = os.environ['WW_BOT_KEY']

        url = f'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key={WW_BOT_KEY}'
        data = {
            'msgtype': 'text',
            'text': {
                'content': f'{text} {status}\n\n{desp}'
            }
        }
        conf = ['Enterprise WeChat Robot', 'WW_BOT_KEY', WW_BOT_KEY, 'errcode', 0]
        name, needs, token, text, code  = conf

        return self.pushTemplate('post', url, json=data, name=name, needs=needs, token=token, text=text, code=code)

    def get_wwtoken(self):
        WW_ID = self.WW_ID
        if 'WW_ID' in os.environ:
            WW_ID = os.environ['WW_ID']

        WW_APP_SECRET = self.WW_APP_SECRET
        if 'WW_APP_SECRET' in os.environ:
            WW_APP_SECRET = os.environ['WW_APP_SECRET']

        if WW_ID and WW_APP_SECRET:
            url = 'https://qyapi.weixin.qq.com/cgi-bin/gettoken'
            data = {
                'corpid': WW_ID,
                'corpsecret': WW_APP_SECRET
            }

            try:
                response = req.to_python(
                    req.request('get', url, params=data).text)
                rspcode = response['errcode']
            except Exception as e:
                log.error(e)
            else:
                if rspcode == 0:
                    log.info('access_token Acquisition successful')
                    return response['access_token']
                else:
                    log.error(f'access_token Acquisition failure:\n{response}')
        else:
            log.info('Enterprise WeChat application 🚫')
            # log.info ('Enterprise WeChat application "WW_ID and WW_APP_SECRET needed to push, are jumping ...')

    def wwApp(self, text, status, desp):
        WW_APP_USERID = self.WW_APP_USERID
        if 'WW_APP_USERID' in os.environ:
            WW_APP_USERID = os.environ['WW_APP_USERID']

        WW_APP_AGENTID = self.WW_APP_AGENTID
        if 'WW_APP_AGENTID' in os.environ:
            WW_APP_AGENTID = os.environ['WW_APP_AGENTID']

        token = ''
        if WW_APP_USERID and WW_APP_AGENTID:
            token = 'token'
        access_token = self.get_wwtoken()

        if access_token:
            url = f'https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token={access_token}'
            data = {
                'touser': WW_APP_USERID,
                'msgtype': 'text',
                'agentid': WW_APP_AGENTID,
                'text': {
                    'content': f'{text} {status}\n\n{desp}'
                }
            }
            conf = ['Enterprise WeChat application', 'WW_APP_USERID with WW_APP_AGENTID', token, 'errcode', 0]
            name, needs, token, text, code  = conf

            return self.pushTemplate('post', url, json=data, name=name, needs=needs, token=token, text=text, code=code)

    def iGot(self, text, status, desp):
        IGOT_KEY = self.IGOT_KEY
        if 'IGOT_KEY' in os.environ:
            IGOT_KEY = os.environ['IGOT_KEY']

        url = f'https://push.hellyw.com/{IGOT_KEY}'
        data = {
            'title': f'{text} {status}',
            'content': desp
        }
        conf = ['iGot', 'IGOT_KEY', IGOT_KEY, 'ret', 0]
        name, needs, token, text, code  = conf

        return self.pushTemplate('post', url, data=data, name=name, needs=needs, token=token, text=text, code=code)

    def pushPlus(self, text, status, desp):
        PUSH_PLUS_TOKEN = self.PUSH_PLUS_TOKEN
        if 'PUSH_PLUS_TOKEN' in os.environ:
            PUSH_PLUS_TOKEN = os.environ['PUSH_PLUS_TOKEN']

        PUSH_PLUS_USER = self.PUSH_PLUS_USER
        if 'PUSH_PLUS_USER' in os.environ:
            PUSH_PLUS_USER = os.environ['PUSH_PLUS_USER']

        url = 'http://www.pushplus.plus/send'
        data = {
            'token': PUSH_PLUS_TOKEN,
            'title': f'{text} {status}',
            'content': desp,
            'topic': PUSH_PLUS_USER
        }
        conf = ['Pushplus', 'PUSH_PLUS_TOKEN', PUSH_PLUS_TOKEN, 'code', 200]
        name, needs, token, text, code  = conf

        return self.pushTemplate('post', url, data=data, name=name, needs=needs, token=token, text=text, code=code)

    def custPush(self, text, status, desp):
        PUSH_CONFIG = self.PUSH_CONFIG
        if 'PUSH_CONFIG' in os.environ:
            PUSH_CONFIG = os.environ['PUSH_CONFIG']

        if not PUSH_CONFIG:
            return log.info(f'Custom Push 🚫')
        cust = req.to_python(PUSH_CONFIG)
        title = f'{text} {status}'
        if cust['show_title_and_desp']:
            title = f'{text} {status}\n\n{desp}'
        if cust['set_data_title'] and cust['set_data_sub_title']:
            cust['data'][cust['set_data_title']] = {
                cust['set_data_sub_title']: title
            }
        elif cust['set_data_title'] and cust['set_data_desp']:
            cust['data'][cust['set_data_title']] = title
            cust['data'][cust['set_data_desp']] = desp
        elif cust['set_data_title']:
            cust['data'][cust['set_data_title']] = title
        conf = [cust['url'], cust['data'], 'Custom push', cust['text'], cust['code']]
        url, data, name, text, code  = conf

        if cust['method'].upper() == 'GET':
            return self.pushTemplate('get', url, params=data, name=name, token='token', text=text, code=code)
        elif cust['method'].upper() == 'POST' and cust['data_type'].lower() == 'json':
            return self.pushTemplate('post', url, json=data, name=name, token='token', text=text, code=code)
        else:
            return self.pushTemplate('post', url, data=data, name=name, token='token', text=text, code=code)

    def discordWebhook(self, text, status, desp):
        DISCORD_WEBHOOK = self.DISCORD_WEBHOOK
        if 'DISCORD_WEBHOOK' in os.environ:
            DISCORD_WEBHOOK = os.environ['DISCORD_WEBHOOK']

        if not DISCORD_WEBHOOK:
            return log.info(f'Discord 🚫')
        colors = [0xFFE4E1, 0x00FF7F, 0xD8BFD8, 0xDC143C, 0xFF4500, 0xDEB887, 0xADFF2F, 0x800000, 0x4682B4, 0x006400, 0x808080, 0xA0522D, 0xF08080, 0xC71585, 0xFFB6C1, 0x00CED1]
        webhook = DiscordWebhook(url=DISCORD_WEBHOOK)
        embed = DiscordEmbed(title=f'{text} · {status}', url='https://github.com/fluteds/gih/actions/workflows/main-os.yml', description=desp, color=random.choice(colors))
        embed.set_timestamp()
        embed.set_footer(text='Generated by Gih', icon_url='https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi.pinimg.com%2Foriginals%2F94%2F9c%2F4e%2F949c4e3bf18b014915911c0d8da1c6f2.jpg&f=1&nofb=1')
        webhook.add_embed(embed)
        response = webhook.execute()
        if (response.status_code == 200):
            log.info(f'Discord 🥳')
        else:
            log.error(f'Discord 😳\n{response}')

    def send(self, **kwargs):
        app = 'Genshin Impact Helper'
        status = kwargs.get('status', '')
        msg = kwargs.get('msg', '')
        hide = kwargs.get('hide', '')
        if isinstance(msg, list) or isinstance(msg, dict):
            # msg = self.to_json(msg)
            msg = '\n\n'.join(msg)
        if not hide:
            log.info(f'Sign-in: {status}\n\n{msg}')
        log.info('Ready to send notification(s)...')

        self.serverChan(app, status, msg)
        self.coolPush(app, status, msg)
        self.barkPush(app, status, msg)
        self.tgBot(app, status, msg)
        self.ddBot(app, status, msg)
        self.wwBot(app, status, msg)
        self.wwApp(app, status, msg)
        self.iGot(app, status, msg)
        self.pushPlus(app, status, msg)
        self.custPush(app, status, msg)
        self.discordWebhook(app, status, msg)


if __name__ == '__main__':
    Notify().send(app='Genshin Impact Helper', status='Sign-in', msg='Details')
