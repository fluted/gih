import os
import re

from time import sleep
from bs4 import BeautifulSoup

from settings import log, CONFIG, req
from notify import Notify


class Weibo():
    def __init__(self, wb_cookie: str = None):
        self.wb_cookie = wb_cookie

    def get_header(self):
        header = {
            'User-Agent': CONFIG.WB_USER_AGENT,
            'Referer': 'https://m.weibo.cn',
            'Cookie': self.wb_cookie
        }
        return header

    def get_super_list(self):
        log.info('Prepare to get a list of devices...')
        try:
            response = req.request('get', CONFIG.SUPER_URL,
                headers=self.get_header(), allow_redirects=False)
        except Exception as e:
            log.error(e)
        else:
            if response.status_code == 200:
                log.info('🥳 weibo: Superphracted list acquisition success')
                return req.to_python(response.text)
            elif response.status_code == 302:
                log.error('😳 Weibo: Login may fail, try to log in again')
            else:
                log.error('😳 Weibo: Super words list acquisition failed')

        log.info('Super words list')
        return

    def resolve_data(self):
        super_list = self.get_super_list()
        if not super_list:
            log.info('Cancel the analysis of data information')
            return
        log.info('Prepare to parse data information...')
        follow_list = []
        try:
            card_group = super_list['data']['cards'][0]['card_group'][1:-1]
        except Exception as e:
            log.error(e)
        else:
            for card in card_group:
                follow = {
                    'title_sub':card['title_sub'],
                    'containerid': card['scheme'].split('&')[0].split('=')[1],
                    'lv': int(re.findall('\d+', card['desc1'])[0]),
                    'is_sign': card['buttons'][0]['name'],
                    'sign_url': False
                }
                scheme = card['buttons'][0]['scheme']
                if scheme:
                    follow['sign_url'] = f'https://m.weibo.cn{scheme}'
                follow_list.append(follow)
        log.info(f'Resolve {len(follow_list)} article data')
        if follow_list:
            for follow in follow_list:
                log.info(f'⚜️ [Lv.{follow["lv"]}]{follow["title_sub"]}')

        log.info('Data information parsing is completed')
        return follow_list

    def super_sign(self):
        follow_list = self.resolve_data()
        if not follow_list:
            log.info('Remove Weibo Super Tutorial Sign')
            return
        for follow in follow_list:
            lv = f'[Lv.{follow["lv"]}]'
            name = follow['title_sub']
            log.info(f'Ready for {name} Overturning...')
            sleep(5)
            if follow['is_sign'] == 'Is signed in':
                log.info(f'👀 {lv}{name}: Has been signed in')
                continue
            elif follow['is_sign'] == 'Sign in':
                url = follow['sign_url']
                try:
                    response = req.to_python(req.request(
                        'post', url, headers=self.get_header()).text)
                except Exception as e:
                    log.error(e)
                else:
                    if response['ok'] == 1:
                        log.info(f'🥳 {lv}{name}: Sign in success')
                    else:
                        log.info(f'😳 {lv}{name}: Sign in failure\n{response}')

        log.info('Weibo is completed')
        return


class RedeemCode(object):
    def __init__(self, ka_cookie: str = None):
        self.ka_cookie = ka_cookie
        self.header = Weibo().get_header()

    def get_id(self):
        log.info('Preparing to obtain activity information...')
        id_list = []
        try:
            response = req.to_python(req.request(
                'get', CONFIG.YS_URL, headers=self.header).text)
            group = response['data']['cards'][3]['card_group'][0]['group']
        except Exception as e:
            log.error(f'Activity information failure:\n{e}')
        else:
            for ids in group:
                if 'Package' in ids.get('title_sub', ''):
                    id = re.findall('(?<=gift\/)(.*)\?channel', ids['scheme'])[0]
                    log.info(f'└─🎁 {ids["title_sub"]}')
                    id_list.append(id)
            if not id_list:
                log.info('Original God overtime')

        log.info('The activity information is acquired')
        return id_list

    def get_code(self, id):
        item = f'🎁 {id}'
        log.info(f'Ready to receive {item} redemption code...')
        data = {
            'gid': 10725,
            'itemId': id,
            'channel': 'wblink'
        }
        self.header['Referer'] = f'https://ka.sina.com.cn/html5/gift/{id}'
        self.header['Cookie'] = self.ka_cookie

        retry = 3
        sec = 5
        for i in range(retry):
            sleep(sec)
            log.info(f'♻️ First {i + 1} to receive {id} redemption code...')
            try:
                response = req.to_python(req.request(
                    'get', CONFIG.KA_URL, params=data, headers=self.header).text)
            except Exception as e:
                log.error(e)
            else:
                if response.get('k'):
                    log.info(f'{item} Redemption code to receive success')
                    return response['data']['kahao']
                elif response.get('code') == '2002' and 'Avatar' in response.get('msg', ''):
                    log.error(f'🥳 {id}: I can only receive once or the redemption code has already been received.')
                    break
                elif response.get('code') == '2002' and 'Sign in' or 'Not yet' in response.get('msg', ''):
                    log.error(f'😳 {id}: {response["msg"]}')
                    break
                elif response.get('code') == '2002':
                    log.error(f'😳 {id}: {response["msg"]}')
                elif 'login' in response.get('msg', ''):
                    log.error('Login failure, please login again')
                    return
                else:
                    log.error(f'😳 {id}: {response}')

                if i + 1 != retry:
                    log.info(f'will be {sec} Retry after the timer runs out...')
                else:
                    log.error(f'🚫 {id}: Failure! Number reached the upper limit, Aborting redeem code')

        log.info('The redemption code is acquired')
        return

    def get_box_code(self):
        log.info('Preparing to get the redemption code of "Personal Center"...')
        id_list = []
        code_list = []
        self.header['Referer'] = f'https://ka.sina.com.cn/html5/'
        self.header['Cookie'] = self.ka_cookie
        try:
            response = req.request('get',
                CONFIG.BOX_URL, headers=self.header, allow_redirects=False)
        except Exception as e:
            log.error(e)
        else:
            if response.status_code == 200:
                response.encoding = 'utf-8'
                soup = BeautifulSoup(response.text, 'html.parser')
                # print(soup.prettify())
                boxs = soup.find_all(class_ = 'giftbag')
                for box in boxs:
                    item = {
                        'id': box.find(class_ = 'deleBtn').get('data-itemid'),
                        'title': box.find(class_ = 'title itemTitle').text,
                        'code': box.find('span').parent.contents[1]
                    }
                    log.info(f'└─☁️{item["title"]}')
                    id_list.append(item['id'])
                    code_list.append(item)
                code_list.insert(0, id_list)
            elif response.status_code == 302:
                log.error('😳 ka.sina: Login may fail, try to log in again')
            else:
                log.error('😳 ka.sina: Redemption code acquisition failed')

        # Print exchange code
        # print(req.to_json(code_list))

        log.info('"Personal Center" redemption code is acquired')
        return code_list


if __name__ == '__main__':
    log.info(f'🌀 Weibo Super Dismant Signing Assistant v{CONFIG.WBH_VERSION}')
    """Sina COOKIE
    :Param WB_Cookie: Cookie of Sina Weibo. Go to https://m.weibo.cn.
    :PARAM KA_COOKIE: Cookie of Sina Novice Card. Go to https://ka.sina.com.cn.
    """
    # GitHub Actions Users Please go to the settings-> secrets of the REPO to set the variable, the variable name must be completely consistent with the above parameter variable name, otherwise it will be invalid !!!
    # Name = <Variable Name>, value = <Get Value>
    WB_COOKIE = ''
    KA_COOKIE = ''

    if os.environ.get('WB_COOKIE', '') != '':
        WB_COOKIE = os.environ['WB_COOKIE']
    if os.environ.get('KA_COOKIE', '') != '':
        KA_COOKIE = os.environ['KA_COOKIE']

    if WB_COOKIE:
        Weibo(WB_COOKIE).super_sign()
    if KA_COOKIE:
        events = RedeemCode(KA_COOKIE).get_id()
        codes = RedeemCode(KA_COOKIE).get_box_code() if events else ''
        if events and codes:
            ids = [i for i in events if i not in codes[0]]
            if not ids:
                log.info('The redemption code has been received')
            else:
                log.info(f'Detected {len(ids)} unclaimed redemption code(s)')
                for id in ids:
                    code = RedeemCode(KA_COOKIE).get_code(id)
                    status = 'Original god exchange code' if code else 'Original God Weibo Campaign reminder'
                    msg = code if code else f'🎁 You have unclaimed gift packages'
                    Notify().send(status=status, msg=msg, hide=True)

        else:
            log.info('Cancelled pick-up package')

