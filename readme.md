<div align="center">
<h1 align="center">Genshin Impact Helper</h1>

<div align="center">Automatically collect daily check-in rewards from HoYoLAB Community and Weibo via Docker.
</div>

## Features

- **Automatic sign-in**
<br>The program will automatically execute the sign-in process every morning, or you can go through the deployment  at any time via a manual trigger, refer to the specific time [here](.github/workflows/main.yml)
- **Docker sign-in**
<br>Supports Docker usage
- **Support synchronization**
<br>Automatically synchronize forks, disabled by default
- **Push notification support**
<br>Optional multiple notification/subscription methods that push the check-in results to any device
- **Support multiple accounts**
<br>Use `#` between values such as: `Cookie1#Cookie2#Cookie3`
- **Weibo Sign-in**
<br>Supports signing into Weibo and claiming exclusive rewards

## Contributing

Please open any pull requests or issues if you want to contribute to translation or maintaining this repository. I try my best to keep on top of the changing API but can't do it without your help

## Agreement

Using Genshin Impact Helper means that you know and agree to:

- This code uses cookies to log in to the Mihoyo webpage through a simulated browser, and clicks the page to collect the sign-in. The function is implemented through the official public API, not a game plug-in or extension.
- The user's cookies are stored on the Github server and are only used for this project. If the Github server is compromised, your cookies are at risk of being leaked. In addition to this, the developer does not have the right to obtain your cookies; even the user, once you have created `Secrets`, you can’t view the cookies from it again after adding.
- It is strongly reccomended to use this service on an alternative account.
- Genshin Impact Helper will not be responsible for any of your losses.
