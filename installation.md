# Installation

## Deploy

1. Fork
2. Get your Cookies
3. Add your Cookies to Secrets
4. Enable Actions
5. Run Workflow

<details>
<summary><strong>How To Install</strong></summary>

### 1. Fork Repo

- Click on the upper right corner `Fork`
- Go to your profile
- Go to your newly forked repo

![Fork](https://i.loli.net/2020/10/28/qpXowZmIWeEUyrJ.png)

- Set the default branch of the repo to the `master` branch.

### 2. Get Your Cookies

Open the following links in your browser according to which website you use:

- **Chinese Version:** <https://bbs.mihoyo.com/ys/>
- **English (International Version):** [webstatic-sea.mihoyo.com](https://webstatic-sea.mihoyo.com/ys/event/signin-sea/index.html?act_id=e202102251931481&lang=en-us)

Log in to your account.

#### 2.1 Method One

- Press `F12`, turn on/enable `Developer Tools` and go to the `Network` tab.
- Press `F5` refresh the page and copy as shown below this is your `Cookie`.

![Cookie](https://i.loli.net/2020/10/28/TMKC6lsnk4w5A8i.png)

- When using the `Debugger`, you can try to press `Ctrl + F8` close, then refresh the page again, and finally copy the `Cookie`

#### 2.2 Method Two

- Copy the following code

```js
    var cookie = document.cookie;
    var ask = confirm('Cookie:' + cookie + '\n\nDo you want to copy the cookie contents to the clipboard?');
    if (ask == true) {
        copy(cookie);
        msg = cookie;
    } else {
        msg = 'Cancel';
    }
```

- Press `F12`, turn on `Developer Tools`, go to the `Console` tab and click
- Paste the code on the command line and run it to get something like `Cookie:xxxxxx` Output information
- `xxxxxx` Part is what needs to be copied `Cookie`, Click OK to copy

### 3. Add Cookies to Secrets

- Back to the project page, click `Settings` -> `Secrets` -> `New secret`

![new-secret.png](https://i.loli.net/2020/10/28/sxTuBFtRvzSgUaA.png)

**If you are using the English (International) Version:**

- Create a secret called `OS_COOKIE` and paste the value obtained in `Step 2` finally click `Add secret`
- The secret name must be `OS_COOKIE`！

**If you are using the Chinese Version:**

- Create a secret called `COOKIE` and paste the value obtained in `Step 2` finally click `Add secret`
- The secret name must be `COOKIE`！

![add-secret](https://i.loli.net/2020/10/28/sETkVdmrNcCUpgq.png)

### 4. Enable Actions/Workflows

Actions are disabled by default. After the fork, you need to enable the action then execute it manually, it will be activated if it runs successfully.

Return to the main page of the project and click on the `Actions`, Then click on `Genshin Impact Helper Global` (for English users) or `Genshin Impact Helper` (for other users) Then click `Run workflow`

![Run](https://i.loli.net/2020/10/28/5ylvgdYf9BDMqAH.png)

At this point, the deployment is complete.

</details>

## Results

When you have completed the above process, you can go to the `Actions` page click `Genshin Impact Helper` -> `build` -> `Run sign` Check the running log, pay attention to the `Check in Result` logs.

<details>
<summary><strong>View Results</strong></summary>

### Sign in Success

If successful, it will output something like `Sign-in result: Success: 1 | Failure: 0` Information:

```sh
Check-in result: Success: 1 | Failure: 0

      NO.1 account:
         #########2021-01-13#########
         🔅[Sky Island]1******9
         Today's reward: Mora × 8000
         Total sign ins this month: 13 days
         Check-in result: OK
         ###########################
         #########2021-01-13#########
         🔅[World Tree]5******1
         Today's Reward: Good Ore for Fine Forging × 3
         Total sign ins this month: 2 days
         Check-in result: OK
         ###########################
```

### Sign in Failed

If it fails, it will output something like `Sign-in result: Success: 0 | Failure: 1` Information:

```sh
Check-in result: Success: 0 | Failure: 1

    NO.1 account:
        Login invalid, please log in again
```

At the same time, you will receive an email with the title `Run failed: Genshin Impact Helper - master`.

</details>

Note: If you turn on action subscription push notifications, you will receive push notifications regardless of success or failure.

## Syncing

Because there may be some changes in the API interface, the upstream source code might need to be changed to adapt to these changes. If you do not synchronize the project source code in time, it may cause sign-in failure.

If you are not familiar with syncronizing upstream branches, it is recommended to delete your Fork (`Settings -> Options -> Danger Zone -> Delete this repository`) and re-fork to synchronize updates. Don’t mess with Pull Requests if you're unfamiliar with them.

<details>
<summary><strong>⚠️ If you're using automatic synchronization please read the existing risks</strong></summary>
<p></p>

Possibility of users being comprimised by other third parties if the developer's/users accounts are leaked, which is not clearly stated in the agreement on the homepage. The agreement also contains the statement "In addition, the developer has no right to obtain your cookies", and in fact the developer/third party could collect users data without authorization by changing the source code and then having the user automatically update their fork/branch. A good rule of thumb is that users should always perform code reviews by default when using this software, and then manually update the PR in their own Repo to suit needs.
</details>

If you understand and accept the possible risks of automatic synchronization, please continue reading:

<details>
<summary><strong>How to Turn on Automatic Syncing</strong></summary>
<p></p>

The following re-enables the automatic synchronization function, which is disabled by default.

Synchronization defaults to using the remote branch to overwrite the replica branch. If you want to keep your own changes, you can edit `pull.yml` File, will `mergeMethod: hardreset` change into `mergeMethod: merge`。

### Activate/Install automatic branch synchronization

1. Go to `https://pull.git.ci/check/${owner}/gih` Activate the profile. Where `${owner}` is used. Replace with your username
2. Install [![&lt;img src="https://prod.download/pull-18h-svg" valign="bottom"/> Pull](https://prod.download/pull-18h-svg) Pull app](https://github.com/apps/pull), Select on the installation wizard page `Only select repositories`, Drop-down list selection and select `gih`, Click `Install`. Finish installation
3. The program will automatically synchronize within 3 hours when there is an update in the upstream branch.

### Manually triggering/enabling branch synchronization

After completing the activation and installation, you can go to `https://pull.git.ci/process/${owner}/gih` To manually trigger synchronization, where `${owner}` is used again, modify it to your username. The web page displays `Success` when the trigger is successful.

If there is no automatic synchronization, you should check whether your branch is already up-to-date; or check the branches `Pull Requests`
If there are any [pull] The merge request at the beginning, you need to click to find it `Merge pull request` Button, click to confirm the merge.

</details>

## Subscribe

If you turn on subscription notifications, you will receive workflow push notifications regardless of success or failure.

### Custom Push Notifications

Supports:

- Server Sauce
- Cool Push
- Bark App
- Telegram Bot
- Dingding Robot
- Enterprise WeChat Robot
- Enterprise WeChat Application
- iGot Aggregate Push
- Pushplus
- Custom push for single or multiple pushes

Configure the corresponding parameters to the corresponding push method.

<details><summary><strong>Custom Push Notification Example using Server Sauce</strong></summary>

### Server Sauce

Take Server Sauce as an example:

#### 1. Get Your SendKey

- Enter ["Server Sauce"](https://sct.ftqq.com/) Click on the upper right corner to log in WeChat scan code to log in
- Click ["Message Channel"](https://sct.ftqq.com/forward) Complete the configuration according to the prompts on the page
- Click ["SendKey"](https://sct.ftqq.com/sendkey) and obtain your `SendKey` value

#### 2. Add SendKey to Secrets

- Create a secret called `SCKEY`, and paste the `SendKey` value into it

</details>

For other API notifications, the parameter values list is detailed below.

### Custom Push Notification Config

Create a `PUSH_CONFIG` secret, and fill in the following json template according to the custom push documentation that you use.

```json
{
    "method": "post",
    "url": "",
    "data": {},
    "text": "",
    "code": 200,
    "data_type": "data",
    "show_title_and_desp": false,
    "set_data_title": "",
    "set_data_sub_title": "",
    "set_data_desp": ""
}
```

```json
Description:
    method: required, request method. Default: post.
    url: required, complete custom push link.
    data: Optional, sent data. The default is empty, you can add additional parameters.
    text: required, the key of the status code returned by the response body. For example: the server sauce is errno.
    code: required, the value of the status code returned by the response body. For example, the value of the server sauce is 0.
    data_type: Optional, the method of sending data, optional params|json|data, default: data.
    show_title_and_desp: Optional, whether to merge the title (application name + running status) and the running result. Default: false.
    set_data_title: Required, fill in the key of the message title in the push method data. For example: server sauce is text.
    set_data_sub_title: Optional, fill in the key of the message body in the push method data. Some push methods have a secondary structure in the body key, and need to cooperate with set_data_title to construct a child, which is mutually exclusive with set_data_desp. For example: In enterprise WeChat, set_data_title fills in text, set_data_sub_title Fill in content.
    set_data_desp: Optional, fill in the key of the message body in the push method data. For example, the server sauce is desp. It is mutually exclusive with set_data_sub_title. If both are filled, this item will not take effect.
```

For other push methods, please refer to the corresponding official documents to obtain parameters such as `KEY` or `TOKEN`, and then add them to the `Secrets` tab.

## Parameters

In `Settings`-->`Secrets` The parameters added in must be one of the following parameter names.

The list of required and optional parameter values are as follows.

| parameter name  | Is it required | Defaults           | Description                                                                                                                                                        |
| --------------- | -------------- | ------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| COOKIE          | ✅              |                    | Mihoyo's Chinese Version Cookie                                                                                                                                                  |
| OS_COOKIE       | ✅              |                    | Mihoyo International Version (English) Cookies                                                                                                                             |
| WB_COOKIE       | ❌              |                    | Cookies on Sina Weibo                                                                                                                                              |
| KA_COOKIE       | ❌              |                    | Cookies in Sina Novice Card Center                                                                                                                                 |
| SCKEY           | ❌              |                    | Server Sauce SCKEY                                                                                                                                               |
| COOL_PUSH_SKEY  | ❌              |                    | Cool Push SKEY                                                                                                                                                     |
| COOL_PUSH_MODE  | ❌              | send               | Cool Push push method. Optional private chat (send), group (group) or WeChat (wx).                                                                                 |
| BARK_KEY        | ❌              |                    | Bark's IP or device code                                                                                                                                           |
| BARK_SOUND      | ❌              | healthnotification | Bark's push ringtones. View the list of ringtones in the APP                                                                                                       |
| TG_BOT_API      | ❌              | api.telegram.org   | Telegram API address (can be customized as a reverse proxy server)                                                                                                 |
| TG_BOT_TOKEN    | ❌              |                    | Telegram Bot token. Generated when applying for bot from bot father                                                                                                |
| TG_USER_ID      | ❌              |                    | The user ID of the Telegram push object                                                                                                                            |
| DD_BOT_TOKEN    | ❌              |                    | The field after the access_token in the Dingding robot WebHook address                                                                                             |
| DD_BOT_SECRET   | ❌              |                    | Dingding affixing the key. On the robot security settings page, the string starting with SEC displayed under the signature column                                  |
| WW_BOT_KEY      | ❌              |                    | The field after the key in the WebHook address of the enterprise WeChat robot                                                                                      |
| WW_ID           | ❌              |                    | The corporate ID (corpid) of the corporate WeChat. Check it in `Backstage Management` -> `My Company` -> `Corporate Information`                                        |
| WW_APP_SECRET   | ❌              |                    | The secret of the enterprise WeChat application. Go to the `Administration Backstage` -> `Apps and Mini Programs` -> `Apps` -> `Self-built`, click into an app to view |
| WW_APP_USERID   | ❌              | @all               | The user ID of the target that the enterprise WeChat application pushes. In the `Administration Backstage` -> `Contacts`, click into a user's details page to view   |
| WW_APP_AGENTID  | ❌              |                    | The agentid of the enterprise WeChat application. In the `Administration Backstage` -> `Apps and Mini Programs` -> `Apps`, click into an application to view          |
| IGOT_KEY        | ❌              |                    | iGot's KEY                                                                                                                                                         |
| PUSH_PLUS_TOKEN | ❌              |                    | Pushplus one-to-one push or one-to-many push token                                                                                                                 |
| PUSH_PLUS_USER  | ❌              | One-to-one push    | Pushplus one-to-many push group coding                                                                                                                             |
| PUSH_CONFIG     | ❌              |                    | Custom push configuration in JSON format. For details, please refer to the description of Subscription-Custom Push                                                 |
| CRON_SIGNIN     | ❌              | 30 9 \* \* \*      | Automatic check-in scheduled task of DOCKER script                                                                                                                 |
| DISCORD_WEBHOOK     | ❌              |      | Complete url of Discord webhook application                                                                                                                  |

## Development

If you need to develop or add additional functions, please refer to the following data:

<details>
<summary><strong>View Data Variables</strong></summary>

```python
# Role Information
roles = Roles(cookie).get_roles()
roles = {
    'retcode': 0,
    'message': 'OK',
    'data': {
        'list': [
            {
                'game_biz': 'hk4e_cn',
                'region': 'cn_gf01',
                'game_uid': '111111111',
                'nickname': '酸柚子',
                'level': 48,
                'is_chosen': False,
                'region_name': 'EU',
                'is_official': True
            }
        ]
    }
}
```

```python
# Sign-in information
infos = Sign(cookie).get_info()
infos = [
    {
        'retcode': 0,
        'message': 'OK',
        'data': {
            'total_sign_day': 5,
            'today': '2021-01-05',
            'is_sign': True,
            'first_bind': False,
            'is_sub': False,
            'month_first': False
        }
    }
]
```

</details>

## Agreement

Using Genshin Impact Helper means that you know and agree to:

- This code uses cookies to log in to the Mihoyo webpage through a simulated browser, and click the page to complete the sign-in to collect the sign-in. The function is implemented through the official public API, not a game plug-in or extension.
- The user's cookies are stored on the website server and are only used for this project. If the server is compromised, your cookies are at risk of being leaked. In addition to this, the developer does not have the right to obtain your cookies; even the user, once you have created `Secrets`, you can’t view the cookies from it again after adding.
- Genshin Impact Helper will not be responsible for any of your losses, including but not limited to, reward recovery and abnormal account activity.
